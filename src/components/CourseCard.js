import {useState, useEffect} from 'react';

import {Link} from 'react-router-dom';

import { Button, Row, Col, Card } from 'react-bootstrap';


export default function CourseCard({course}) {

  // Deconstruct the course properties into their own variables
  const { name, description, price, _id } = course;

  /*
  SYNTAX:
    const [getter, setter] = useState(initialGetterValue);
  */
//   const [count, setCount] = useState(0);
//   const [seats, setSeats] = useState(5);

//   function enroll () {
//     // if (count == 0){
//     //   return alert("No more seats");
//     // } else {
//     //   setCount(count - 1)
//     // }
//     setCount(count + 1);
//     setSeats(seats - 1);
//   };

// useEffect(() => {
//     if(seats <= 0){
//       alert("No more seats available!")
//     }
// }, [seats]);



  return (
    <Row className="mt-3 mb-3">
      <Col xs={12}>
        <Card className="cardHighlight p-0">
          <Card.Body>
            <Card.Title><h4>{name}</h4></Card.Title>
            <Card.Subtitle>Description</Card.Subtitle>
            <Card.Text>{description}</Card.Text>
            <Card.Subtitle>Price</Card.Subtitle>
            <Card.Text>{price}</Card.Text>
            {/*<Card.Subtitle>Enrollees</Card.Subtitle>
            <Card.Text>{count} Enrollees</Card.Text>
            <Card.Text>{seats} Seats</Card.Text>*/}
            
            {/*<Button variant="primary" onClick={enroll} disabled={seats<=0}>Enroll</Button>*/}
            <Button className="bg-primary" as={Link} to={`/courses/${_id}`}>Details</Button>
          </Card.Body>
        </Card>
      </Col>
    </Row>
  );
}
