import { Button, Row, Col } from 'react-bootstrap';
import {NavLink} from 'react-router-dom';

export default function ErrorBanner() {
return (
    <Row>
    	<Col className="p-5">
            <h1>Error 404 - Page not found.</h1>
            <p>The page you are looking for cannt be found.</p>
            <Button variant="primary" as={NavLink} to="/home">Back to Home</Button>
        </Col>
    </Row>
	)
}