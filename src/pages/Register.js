import { useState, useEffect, useContext } from 'react';

import Swal from 'sweetalert2';

import UserContext from '../UserContext';

import {Navigate, useNavigate, useParams} from 'react-router-dom';

import { Form, Button } from 'react-bootstrap';

export default function Register() {

	const {user} = useContext(UserContext);

	const navigate = useNavigate();

	const {userId} = useParams();

	// to store and manage value of the input fields
	const [firstName, setFirstName] = useState("");
	const [lastName, setLastName] = useState("");
	const [mobileNo, setMobileNo] = useState("");
	const [email, setEmail] = useState("");
	const [password1, setPassword1] = useState("");
	const [password2, setPassword2] = useState("");
	// to determine whether submit button is enabled or not
	const [isActive, setIsActive] = useState(false);

	// const register = (userId) => {
		
	// };

	useEffect(() => {
		if (( email !== "" && password1 !== "" && password2 !== "" && firstName !== "" && lastName !== "" && mobileNo !== "") && password1 === password2 && mobileNo.length >= 11) {
			setIsActive(true);
		} else {
			setIsActive(false);
		};
	}, [email, password1, password2, firstName, lastName, mobileNo]);

	// function to simulate user registration
	function registerUser(e) {

		e.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`,{
			method: "POST",
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if(data === true) {
				Swal.fire({
					title: "Duplicate email found",
					icon: "error",
					text: "Please provide a different email."
				})
			
			} else {
				fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
			method: "POST",
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				firstName: firstName,
				lastName: lastName,
				mobileNo: mobileNo,
				email: email,
				password: password1
				})
			})
		.then(res => res.json())
		.then(data => {
			console.log(data)

					Swal.fire({
					title: "Successfully Registered!",
					icon: "success",
					text: "You have successfully registered. Enroll a course now!"
				})

				navigate("/login")
			})
			}
		})
		// Clear input fields
		setFirstName("");
		setLastName("");
		setMobileNo("");
		setEmail("");
		setPassword1("");
		setPassword2("");
		}


    return (
    	(user.id !== null) ?
        <Navigate to = "/courses" />
        :
        <Form onSubmit={(e) => registerUser(e)} >
        	<Form.Group controlId="firstName">
                <Form.Label>First Name</Form.Label>
                <Form.Control 
	                type="name" 
	                placeholder="First Name" 
	                value={firstName}
	                onChange={e => setFirstName(e.target.value)}
	                required
                />
            </Form.Group>

            <Form.Group controlId="lastName">
                <Form.Label>Last Name</Form.Label>
                <Form.Control 
	                type="name" 
	                placeholder="Last Name" 
	                value={lastName}
	                onChange={e => setLastName(e.target.value)}
	                required
                />
            </Form.Group>

            <Form.Group controlId="mobileNo">
                <Form.Label>Mobile Number</Form.Label>
                <Form.Control 
	                type="number" 
	                placeholder="Mobile Number" 
	                value={mobileNo}
	                onChange={e => setMobileNo(e.target.value)}
	                required
                />
            </Form.Group>

            <Form.Group controlId="userEmail">
                <Form.Label>Email address</Form.Label>
                <Form.Control 
	                type="email" 
	                placeholder="Enter email" 
	                value={email}
	                onChange={e => setEmail(e.target.value)}
	                required
                />
                <Form.Text className="text-muted">
                    We'll never share your email with anyone else.
                </Form.Text>
            </Form.Group>

            <Form.Group controlId="password1">
                <Form.Label>Password</Form.Label>
                <Form.Control 
	                type="password" 
	                placeholder="Password" 
	                value={password1}
	                onChange={e => setPassword1(e.target.value)}
	                required
                />
            </Form.Group>

            <Form.Group controlId="password2">
                <Form.Label>Verify Password</Form.Label>
                <Form.Control 
	                type="password" 
	                placeholder="Verify Password" 
	                placeholder="Password" 
	                value={password2}
	                onChange={e => setPassword2(e.target.value)}
	                required
                />
            </Form.Group>

        	{isActive ?
				<Button variant="primary" type="submit" id="submitBtn">
				Submit
				</Button>
        		:
        		<Button variant="danger" type="submit" id="submitBtn" disabled>
				Submit
				</Button>
        	}
            
        </Form>
    )

}

